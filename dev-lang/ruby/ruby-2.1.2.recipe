SUMMARY="A programming language focused on simplicity and productivity"
DESCRIPTION="
Ruby is a dynamic, reflective, object-oriented, general-purpose
programming language. It was designed and developed in the mid-1990s
by Yukihiro 'Matz' Matsumoto in Japan.

Ruby embodies syntax inspired by Perl with Smalltalk-like features and
was also influenced by Eiffel and Lisp.  It supports multiple
programming paradigms, including functional, object oriented, and
imperative. It also has a dynamic type system and automatic memory
management. Therefore, it is similar in varying degrees to, Smalltalk,
Python, Perl, Lisp, Dylan, and CLU."
HOMEPAGE="http://www.ruby-lang.org" 
SRC_URI="http://cache.ruby-lang.org/pub/ruby/2.1/ruby-2.1.2.tar.gz"
CHECKSUM_SHA256="f22a6447811a81f3c808d1c2a5ce3b5f5f0955c68c9a749182feb425589e6635"
LICENSE="Ruby
	BSD (3-clause)"
COPYRIGHT="1993-2014 Yukihiro Matsumoto"

REVISION="1"

ARCHITECTURES="?x86 !x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	ruby$secondaryArchSuffix = $portVersion compat >= 2.1
	cmd:ruby$secondaryArchSuffix = $portVersion compat >= 2.1
	cmd:erb$secondaryArchSuffix = $portVersion compat >= 2.1
	cmd:gem$secondaryArchSuffix = $portVersion compat >= 2.1
	cmd:irb$secondaryArchSuffix = $portVersion compat >= 2.1
	cmd:rake$secondaryArchSuffix = $portVersion compat >= 2.1
	cmd:rdoc$secondaryArchSuffix = $portVersion compat >= 2.1
	cmd:ri$secondaryArchSuffix = $portVersion compat >= 2.1
	cmd:testrb$secondaryArchSuffix = $portVersion compat >= 2.1
	lib:libruby$secondaryArchSuffix = $portVersion compat >= 2.1
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libedit$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	lib:libcrypto$secondaryArchSuffix
	lib:libssl$secondaryArchSuffix
	lib:libiconv$secondaryArchSuffix
	lib:libncursesw$secondaryArchSuffix
	"
	
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libedit$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	devel:libcrypto$secondaryArchSuffix
	devel:libssl$secondaryArchSuffix
	devel:libiconv$secondaryArchSuffix
	devel:libncursesw$secondaryArchSuffix
	devel:libffi$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:g++$secondaryArchSuffix
	cmd:autoconf
	cmd:bison
	cmd:git
	cmd:libtoolize
	cmd:make
	cmd:pkg_config$secondaryArchSuffix
	"

PROVIDES_devel="
	ruby${secondaryArchSuffix}_devel = $portVersion
	devel:libruby_static$secondaryArchSuffix = $portVersion compat >= 2.1
	lib:libruby_static$secondaryArchSuffix = $portVersion compat >= 2.1
	devel:libruby$secondaryArchSuffix = $portVersion compat >= 2.1
	"

REQUIRES_devel="
	ruby$secondaryArchSuffix == $portVersion base
	"

PATCHES="ruby-2.1.2.patchset"
BUILD()
{
	libtoolize --force --copy --install
	autoconf
	runConfigure ./configure --enable-shared --disable-silent-rules
	# The build process needs to run ruby, and without this it fails to find
	# libruby.so.
	export LIBRARY_PATH=$LIBRARY_PATH:%A
	make $jobArgs Q=''
}

INSTALL()
{
	export LIBRARY_PATH=$LIBRARY_PATH:%A
	make install
	prepareInstalledDevelLibs libruby-static libruby
	packageEntries devel $developDir
}

TEST()
{
	export LIBRARY_PATH=$LIBRARY_PATH:%A
	make check
}
